import java.util.Calendar;

/**
 * Created by p.kovarsky on 11/26/16.
 */
public class Test1 {
    public static void main(String[] args) {

        // Task 1

        int germanyPopulation = 80_620_000;
        int brazilPopulation = 190_710_111;
        int kubaPopulation = 11_163_934;

        System.out.println("Task 1" + "\n\n" + "Germany's Population: " + germanyPopulation + " peoples" + "\n" + "Brazil's Population: " + brazilPopulation +
                " peoples" + "\n" + "Kuba's Population: " + kubaPopulation + " peoples");

        // Task 2
        double germanyArea = 357_168;
        double brazilArea = 8_516_000;
        double kubaArea= 109_884;

        System.out.println("\n\n" + "Task 2" + "\n\n" + "Germany's Area:" + "\n" + "In square meters: " + germanyArea * 1000000
                + "\n" + "In square miles: " + germanyArea * 0.386102 + "\n\n" + "Brazil's Area:" + "\n" + "In square meters: " + brazilArea * 1000000
                + "\n" + "In square miles: " + brazilArea * 0.386102 + "\n\n" + "Kuba's Area:" + "\n" + "In square meters: " + kubaArea * 1000000
                + "\n" + "In square miles: " + kubaArea * 0.386102);

        // Task 3

        double[] anExchangeRates = {27.05, 28.82, 0.4147};
        double thesalary = 5000;
        System.out.println("\n\n" + "Task 3" + "\n\n" + "Salary in dollars: " + thesalary * anExchangeRates[0] + "\n"
                + "Salary in Euro: " + thesalary * anExchangeRates[1] + "\n" + "Salary in Ruble: " + thesalary * anExchangeRates[2]);

        // Task 4

        int year = Calendar.getInstance().get(Calendar.YEAR);
        int age = 0;
        String ageManager = "let";
        String[][] names = {
                {
                        "John", "Samara", "Bob"
                },
                {
                        "1993", "1956", "1999"
                }
        };
        int[] ages = {year - Integer.parseInt(names[1][0]), year - Integer.parseInt(names[1][1]), year - Integer.parseInt(names[1][2])};

        if ((0 <= ages[0] && ages[0] <= 4) || 21 == ages[0]
                || 31 == ages[0] || 41 == ages[0]
                || 51 == ages[0] || 61 == ages[0]
                || 71 == ages[0] || 81 == ages[0]) {
            ageManager = "God";
        }

        if ((22 <= ages[0] && ages[0] <= 24) || (32 <= ages[0] && ages[0] <= 34) || (42 <= ages[0] && ages[0] <= 44)
                || (52 <= ages[0] && ages[0] <= 54) || (62 <= ages[0] && ages[0] <= 64) || (72 <= ages[0] && ages[0] <= 74)
                || (82 <= ages[0] && ages[0] <= 84)) {
            ageManager = "Goda";
        }

        System.out.println("\n\n" + "Test 4.1" + "\n\n" + names[0][0] + ": " + ages[0] + " " + ageManager);
        ageManager = "let";

        if ((0 <= ages[1] && ages[1] <= 4) || 21 == ages[1]
                || 31 == ages[1] || 41 == ages[1]
                || 51 == ages[1] || 61 == ages[1]
                || 71 == ages[1] || 81 == ages[1]) {
            ageManager = "God";
        }

        if ((22 <= ages[1] && ages[1] <= 24) || (32 <= ages[1] && ages[1] <= 34) || (42 <= ages[1] && ages[1] <= 44)
                || (52 <= ages[1] && ages[1] <= 54) || (62 <= ages[1] && ages[1] <= 64) || (72 <= ages[1] && ages[1] <= 74)
                || (82 <= ages[1] && ages[1] <= 84)) {
            ageManager = "Goda";
        }

        System.out.println(names[0][1] + ": " + ages[1] + " " + ageManager);
        ageManager = "let";

        if ((0 <= ages[2] && ages[2] <= 4) || 21 == ages[2]
                || 31 == ages[2] || 41 == ages[2]
                || 51 == ages[2] || 61 == ages[2]
                || 71 == ages[2] || 81 == ages[2]) {
            ageManager = "God";
        }


        if ((22 <= ages[2] && ages[2] <= 24) || (32 <= ages[2] && ages[2] <= 34) || (42 <= ages[2] && ages[2] <= 44)
                || (52 <= ages[2] && ages[2] <= 54) || (62 <= ages[2] && ages[2] <= 64) || (72 <= ages[2] && ages[2] <= 74)
                || (82 <= ages[2] && ages[2] <= 84)) {
            ageManager = "Goda";
        }

        System.out.println(names[0][2] + ": " + ages[2] + " " + ageManager);

    }
}